package com.example.ropa.data.network.shirts

import com.example.ropa.data.models.shirts.Shirt
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface ShirtApiClient {

    @GET("shirts")
    suspend fun getAllShirts() : Response<List<Shirt>>

    @GET("shirts/{id}")
    suspend fun getShirt(@Path("id") id: Int) : Response<Shirt>
}