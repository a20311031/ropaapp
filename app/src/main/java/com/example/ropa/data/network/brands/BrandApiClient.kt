package com.example.ropa.data.network.brands

import com.example.ropa.data.models.brands.Brand
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

interface BrandApiClient {

    @GET("brands")
    suspend fun getAllBrands() : Response<List<Brand>>

    @GET("brands/{id}")
    suspend fun getBrand(@Path("id") id: Int) : Response<Brand>
}