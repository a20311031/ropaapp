package com.example.ropa.ui.viewModels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.ropa.domain.GetAllShirts
import com.example.ropa.domain.GetShirt
import com.example.ropa.data.models.shirts.Shirt
import kotlinx.coroutines.launch

class ShirtsViewModel : ViewModel() {

    val shirts = MutableLiveData<List<Shirt>>()
    val shirt = MutableLiveData<Shirt>()
    val isLoading = MutableLiveData<Boolean>()

    val getAllShirts = GetAllShirts()
    val getShirt = GetShirt()

    fun loadShirt(shirt_id: Int) {
        viewModelScope.launch {
            val result = getShirt.invoke(shirt_id)
            isLoading.postValue(true)
            shirt.postValue(result)
            isLoading.postValue(false)
        }
    }

    fun loadShirts(){
        viewModelScope.launch {
            isLoading.postValue(true)
            val result = getAllShirts()

            if(!result.isNullOrEmpty()){
                shirts.postValue(result)
                isLoading.postValue(false)
            }
        }
    }
}