package com.example.ropa.data.network.categories

import com.example.ropa.data.models.categories.Category
import retrofit2.Response
import retrofit2.http.GET

interface CategoryApiClient {

    @GET("categories")
    suspend fun getAllCategories() : Response<List<Category>>
}