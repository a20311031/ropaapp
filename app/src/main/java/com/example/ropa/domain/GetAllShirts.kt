package com.example.ropa.domain

import com.example.ropa.data.repositories.ShirtsRepository

class GetAllShirts {
    private val repository = ShirtsRepository()

    suspend operator fun invoke() = repository.getAllShirts()
}