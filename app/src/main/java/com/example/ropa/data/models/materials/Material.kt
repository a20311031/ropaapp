package com.example.ropa.data.models.materials

import com.google.gson.annotations.SerializedName

data class Material(

    @SerializedName("id") val id: Int,
    @SerializedName("name") val name: String,
    @SerializedName("description") val description: String,
    @SerializedName("properties") val properties: String,
    @SerializedName("status") val status: Int,
    @SerializedName("created_at") val since: String,
)
