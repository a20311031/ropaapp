package com.example.ropa.data.models.categories

import com.google.gson.annotations.SerializedName

data class Category(

    @SerializedName("id") val id: Int,
    @SerializedName("name") val name: String,
    @SerializedName("description") val description: String,
    @SerializedName("image") val image: String,
    @SerializedName("status") val status: Int,
    @SerializedName("created_at") val since: String
)

