package com.example.ropa.data.models.shirts

import com.google.gson.annotations.SerializedName
import com.example.ropa.data.models.categories.Category
import com.example.ropa.data.models.brands.Brand
import com.example.ropa.data.models.materials.Material
import com.example.ropa.data.models.styles.Style



data class Shirt(

    @SerializedName("id") val id: Int,
    @SerializedName("name") val name: String,
    @SerializedName("color") val color: String,
    @SerializedName("size") val size: String,
    @SerializedName("image") val image: String,
    @SerializedName("material") val material: Material,
    @SerializedName("style") val style: Style,
    @SerializedName("brand") val brand: Brand,
    @SerializedName("category") val category: Category,
    @SerializedName("status") val status: Int,
    @SerializedName("created_at") val since: String,
)