package com.example.ropa.ui.viewModels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.ropa.data.models.brands.Brand
import com.example.ropa.domain.GetAllBrands
import com.example.ropa.domain.GetBrand
import kotlinx.coroutines.launch

class BrandsViewModel : ViewModel() {

    val brands = MutableLiveData<List<Brand>>()
    val brand = MutableLiveData<Brand>()
    val isLoading = MutableLiveData<Boolean>()

    val getAllBrands = GetAllBrands()
    val getBrand = GetBrand()

    fun loadBrand(brand_id: Int) {
        viewModelScope.launch {
            val result = getBrand.invoke(brand_id)
            isLoading.postValue(true)
            brand.postValue(result)
            isLoading.postValue(false)
        }
    }

    fun loadBrands(){
        viewModelScope.launch {
            isLoading.postValue(true)
            val result = getAllBrands()

            if(!result.isNullOrEmpty()){
                brands.postValue(result)
                isLoading.postValue(false)
            }
        }
    }
}