package com.example.ropa.data.models.styles

import com.google.gson.annotations.SerializedName

data class Style(

    @SerializedName("id") val id: Int,
    @SerializedName("name") val name: String,
    @SerializedName("description") val description: String,
    @SerializedName("status") val status: Int,
    @SerializedName("created_at") val since: String
)
