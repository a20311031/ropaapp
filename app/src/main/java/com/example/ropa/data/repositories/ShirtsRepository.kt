package com.example.ropa.data.repositories

import com.example.ropa.data.models.shirts.Shirt
import com.example.ropa.data.models.shirts.ShirtProvider
import com.example.ropa.data.models.shirts.ShirtsProvider
import com.example.ropa.data.network.shirts.ShirtService

class ShirtsRepository {
    private val api = ShirtService()

    suspend fun getAllShirts() : List<Shirt> {
        val response = api.getShirts()
        ShirtsProvider.shirts = response
        return response
    }

    suspend fun getShirt(shirt_id: Int) : Shirt {
        val response = api.getShirt(shirt_id)
        ShirtProvider.shirt = response
        return response
    }
}