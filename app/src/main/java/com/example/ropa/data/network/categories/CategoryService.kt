package com.example.ropa.data.network.categories

import com.example.ropa.core.RetrofitHelper
import com.example.ropa.data.models.categories.Category
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class CategoryService {
    private val retrofit = RetrofitHelper.getRetrofit()

    suspend fun getCategories() : List<Category> {
        return withContext(Dispatchers.IO) {
            val response = retrofit.create(CategoryApiClient::class.java)
                .getAllCategories()
            response.body() ?: emptyList()
        }
    }
}