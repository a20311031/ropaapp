package com.example.ropa.ui.views.shirts

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import com.example.ropa.R
import com.example.ropa.databinding.FragmentShirtBinding
import com.example.ropa.ui.viewModels.ShirtsViewModel

class ShirtFragment : Fragment() {

    private var _binding : FragmentShirtBinding?=null
    private val binding get() = _binding!!

    private val shirtsViewModel : ShirtsViewModel by viewModels()
    private var shirtId = 0

    private val args : ShirtFragmentArgs by navArgs()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        _binding = FragmentShirtBinding.inflate(inflater, container, false)
        shirtId = args.shirtId
        shirtsViewModel.loadShirt(shirtId)
        shirtsViewModel.isLoading.observe(viewLifecycleOwner, Observer {
            binding.shirtsShimmer.isVisible=it
            binding.content.isVisible=!it
        })

        shirtsViewModel.shirt.observe(viewLifecycleOwner, Observer {
            shirt->
            binding.shirtName.text=shirt.name
            binding.shirtColor.text=shirt.color
            binding.shirtSize.text=shirt.size
            binding.shirtMaterial.text=shirt.material.name
            binding.shirtStyle.text=shirt.style.name
            binding.shirtBrand.text=shirt.brand.name
            binding.shirtCategory.text=shirt.category.name
        })

        return binding.root
    }


}

