package com.example.ropa.data.network.shirts

import com.example.ropa.core.RetrofitHelper
import com.example.ropa.data.models.shirts.Shirt
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class ShirtService {

    private val retrofit = RetrofitHelper.getRetrofit()

    suspend fun getShirts() : List<Shirt> {

        return withContext(Dispatchers.IO) {
            val response = retrofit.create(
                ShirtApiClient::class.java
            ).getAllShirts()
                response.body() ?: emptyList()
        }
    }

    suspend fun getShirt(shirt_id: Int) : Shirt {

        return withContext(Dispatchers.IO) {
            val response = retrofit.create(
                ShirtApiClient::class.java
            ).getShirt(shirt_id)
            response.body()!!
        }
    }
}