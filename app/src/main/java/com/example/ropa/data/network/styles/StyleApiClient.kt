package com.example.ropa.data.network.styles

import com.example.ropa.data.models.styles.Style
import retrofit2.Response
import retrofit2.http.GET

interface StyleApiClient {

    @GET("styles")
    suspend fun getAllStyles() : Response<List<Style>>
}