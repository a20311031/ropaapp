package com.example.ropa.ui.views.brands

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import com.example.ropa.databinding.FragmentBrandBinding
import com.example.ropa.ui.viewModels.BrandsViewModel


class BrandFragment : Fragment() {

    private var _binding : FragmentBrandBinding?=null
    private val binding get() = _binding!!

    private val brandsViewModel : BrandsViewModel by viewModels()
    private var brandId = 0

    private val args : BrandFragmentArgs by navArgs()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        _binding = FragmentBrandBinding.inflate(inflater, container, false)
        brandId = args.brandId
        brandsViewModel.loadBrand(brandId)
        brandsViewModel.isLoading.observe(viewLifecycleOwner, Observer {
            binding.brandsShimmer.isVisible=it
            binding.content.isVisible=!it
        })

        brandsViewModel.brand.observe(viewLifecycleOwner, Observer {
                brand->
            binding.brandName.text=brand.name
            binding.brandDescription.text=brand.description
            binding.brandCountry.text=brand.country
            binding.brandWebsite.text=brand.website

        })

        return binding.root
    }
}