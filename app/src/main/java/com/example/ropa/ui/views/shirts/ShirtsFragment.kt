package com.example.ropa.ui.views.shirts

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.ropa.R
import com.example.ropa.databinding.FragmentShirtsBinding
import com.example.ropa.ui.viewModels.ShirtsViewModel
import com.example.ropa.ui.views.adapters.ShirtAdapter

class ShirtsFragment : Fragment() {

    private var _binding : FragmentShirtsBinding?=null
    private val binding get() = _binding!!
    private val shirtsViewModel : ShirtsViewModel by viewModels()
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        _binding = FragmentShirtsBinding.inflate(inflater, container, false)
        shirtsViewModel.loadShirts()
        shirtsViewModel.shirts.observe(viewLifecycleOwner, Observer { shirts->
            binding.shirtsRecycler.layoutManager=LinearLayoutManager(context)
            val adapter = ShirtAdapter(shirts)
            binding.shirtsRecycler.adapter = adapter
            if(shirts.isNotEmpty()){

                binding.shirtsShimmer.visibility=View.GONE
                binding.shirtsRecycler.visibility=View.VISIBLE
            }
        })
        shirtsViewModel.isLoading.observe(viewLifecycleOwner, Observer {

            binding.shirtsShimmer.isVisible=it
        })

        return binding.root
    }

}