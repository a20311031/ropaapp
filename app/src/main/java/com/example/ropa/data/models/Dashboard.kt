package com.example.ropa.data.models

data class Dashboard(
    var id: Int,
    var name: String,
    var img: String,
)
