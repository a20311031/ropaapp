package com.example.ropa.ui.views.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.example.ropa.R
import com.example.ropa.data.models.shirts.Shirt
import com.example.ropa.databinding.ItemShirtBinding
import com.example.ropa.ui.views.shirts.ShirtsFragmentDirections

class ShirtAdapter(private val shirts : List<Shirt>) : RecyclerView.Adapter<ShirtAdapter.ShirtsViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int ): ShirtsViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return ShirtsViewHolder(layoutInflater.inflate(
            R.layout.item_shirt, parent, false), parent.context)
    }

    override fun onBindViewHolder(holder: ShirtsViewHolder, position: Int){
        holder.render(shirts[position])

        val shirt : Shirt = shirts[position]
        val binding = ItemShirtBinding.bind(holder.itemView)

        binding.shirtName.text = shirt.name

    }

    override fun getItemCount(): Int {
        return shirts.size
    }

    class ShirtsViewHolder(view: View, ct: Context) : RecyclerView.ViewHolder(view) {

        private val binding = ItemShirtBinding.bind(view)
        private val context = ct

        fun render(shirt: Shirt) {

            binding.shirtCard.setOnClickListener {
                val navController : NavController =
                    Navigation.findNavController(binding.root)

                val action =
                    ShirtsFragmentDirections.actionShirtsFragmentToShirtFragment(shirt.id)
                navController.navigate(action)
            }
        }
    }
}