package com.example.ropa.ui.views.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.example.ropa.R
import com.example.ropa.data.models.brands.Brand
import com.example.ropa.databinding.ItemBrandBinding
import com.example.ropa.ui.views.brands.BrandsFragmentDirections

class BrandAdapter(private val brands : List<Brand>) : RecyclerView.Adapter<BrandAdapter.BrandsViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int ): BrandsViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return BrandsViewHolder(layoutInflater.inflate(
            R.layout.item_brand, parent, false), parent.context)
    }

    override fun onBindViewHolder(holder: BrandsViewHolder, position: Int){
        holder.render(brands[position])

        val brand : Brand = brands[position]
        val binding = ItemBrandBinding.bind(holder.itemView)

        binding.brandName.text = brand.name

    }

    override fun getItemCount(): Int {
        return brands.size
    }

    class BrandsViewHolder(view: View, ct: Context) : RecyclerView.ViewHolder(view) {

        private val binding = ItemBrandBinding.bind(view)
        private val context = ct

        fun render(brand: Brand) {

            binding.brandCard.setOnClickListener {
                val navController : NavController =
                    Navigation.findNavController(binding.root)

                val action =
                    BrandsFragmentDirections.actionBrandsFragmentToBrandFragment(brand.id)
                navController.navigate(action)
            }
        }
    }
}