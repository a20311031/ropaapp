package com.example.ropa.data.network.materials

import com.example.ropa.data.models.materials.Material
import retrofit2.Response
import retrofit2.http.GET

interface MaterialApiClient {

    @GET("materials")
    suspend fun getAllMaterials() : Response<List<Material>>
}