package com.example.ropa.ui.views

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.ropa.R
import com.example.ropa.data.models.Dashboard
import com.example.ropa.databinding.FragmentDashboardBinding
import com.example.ropa.ui.views.adapters.DashboardAdapter

class DashboardFragment : Fragment() {
    private var _binding: FragmentDashboardBinding?=null
    private val binding get() = _binding!!

    private lateinit var dashboardAdapter: DashboardAdapter
    private var options: List<Dashboard> = emptyList()
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        _binding = FragmentDashboardBinding.inflate(inflater,container,false)
        loadData()
        val recyclerView = binding.dashboardRecycler
        dashboardAdapter = DashboardAdapter(options)
        val layoutManager = LinearLayoutManager(context)

        recyclerView.layoutManager = layoutManager

        recyclerView.adapter = dashboardAdapter

        return binding.root
    }

    private fun loadData (){
        val shirt = getString(R.string.dashboard_shirt)
        val material = getString(R.string.dashboard_material)
        val style = getString(R.string.dashboard_style)
        val brand = getString(R.string.dashboard_brand)
        val category = getString(R.string.dashboard_category)
        options = listOf(

            Dashboard(1,shirt,"https://image.spreadshirtmedia.net/image-server/v1/mp/products/T6A231PA5835PT17X4Y121D134289387W30322H3414Cx000000/views/1,width=1200,height=630,appearanceId=231,backgroundColor=F2F2F2,modelId=1405,crop=design/placeholder-mens-t-shirt.jpg"),
            Dashboard(2,brand,"https://image.spreadshirtmedia.net/image-server/v1/mp/products/T6A231PA5835PT17X4Y121D134289387W30322H3414Cx000000/views/1,width=1200,height=630,appearanceId=231,backgroundColor=F2F2F2,modelId=1405,crop=design/placeholder-mens-t-shirt.jpg"),
            Dashboard(3,style,"https://image.spreadshirtmedia.net/image-server/v1/mp/products/T6A231PA5835PT17X4Y121D134289387W30322H3414Cx000000/views/1,width=1200,height=630,appearanceId=231,backgroundColor=F2F2F2,modelId=1405,crop=design/placeholder-mens-t-shirt.jpg"),
            Dashboard(4,category,"https://image.spreadshirtmedia.net/image-server/v1/mp/products/T6A231PA5835PT17X4Y121D134289387W30322H3414Cx000000/views/1,width=1200,height=630,appearanceId=231,backgroundColor=F2F2F2,modelId=1405,crop=design/placeholder-mens-t-shirt.jpg"),
            Dashboard(5,material,"https://image.spreadshirtmedia.net/image-server/v1/mp/products/T6A231PA5835PT17X4Y121D134289387W30322H3414Cx000000/views/1,width=1200,height=630,appearanceId=231,backgroundColor=F2F2F2,modelId=1405,crop=design/placeholder-mens-t-shirt.jpg"),
        )
    }
}