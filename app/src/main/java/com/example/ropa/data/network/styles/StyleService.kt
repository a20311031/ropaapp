package com.example.ropa.data.network.styles

import com.example.ropa.core.RetrofitHelper
import com.example.ropa.data.models.styles.Style
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class StyleService {
    private val retrofit = RetrofitHelper.getRetrofit()

    suspend fun getStyles() : List<Style> {
        return withContext(Dispatchers.IO) {
            val response = retrofit.create(StyleApiClient::class.java)
                .getAllStyles()
            response.body() ?: emptyList()
        }
    }
}