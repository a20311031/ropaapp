package com.example.ropa.data.models.brands

import com.google.gson.annotations.SerializedName

data class Brand(

    @SerializedName("id") val id: Int,
    @SerializedName("name") val name: String,
    @SerializedName("country") val country: String,
    @SerializedName("description") val description: String,
    @SerializedName("website") val website: String,
    @SerializedName("status") val status: Int,
    @SerializedName("created_at") val since: String,

)
