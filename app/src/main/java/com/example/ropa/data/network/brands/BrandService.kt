package com.example.ropa.data.network.brands

import com.example.ropa.core.RetrofitHelper
import com.example.ropa.data.models.brands.Brand
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class BrandService {

    private val retrofit = RetrofitHelper.getRetrofit()

    suspend fun getBrands() : List<Brand> {

        return withContext(Dispatchers.IO) {
            val response = retrofit.create(
                BrandApiClient::class.java
            ).getAllBrands()
            response.body() ?: emptyList()
        }
    }

    suspend fun getBrand(brand_id: Int) : Brand {

        return withContext(Dispatchers.IO) {
            val response = retrofit.create(
                BrandApiClient::class.java
            ).getBrand(brand_id)
            response.body()!!
        }
    }
}