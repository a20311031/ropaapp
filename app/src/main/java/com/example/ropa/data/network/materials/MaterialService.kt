package com.example.ropa.data.network.materials

import com.example.ropa.core.RetrofitHelper
import com.example.ropa.data.models.materials.Material
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class MaterialService {
    private val retrofit = RetrofitHelper.getRetrofit()

    suspend fun getMaterials() : List<Material> {
        return withContext(Dispatchers.IO) {
            val response = retrofit.create(MaterialApiClient::class.java)
                .getAllMaterials()
            response.body() ?: emptyList()
        }
    }
}