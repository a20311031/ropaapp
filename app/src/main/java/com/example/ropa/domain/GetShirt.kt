package com.example.ropa.domain

import com.example.ropa.data.repositories.ShirtsRepository

class GetShirt {

    private val repository = ShirtsRepository()

    suspend operator fun invoke(shirt_id: Int) =
        repository.getShirt(shirt_id)
}