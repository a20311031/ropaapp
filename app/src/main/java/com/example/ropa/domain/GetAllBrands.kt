package com.example.ropa.domain

import com.example.ropa.data.repositories.BrandsRepository

class GetAllBrands {
    private val repository = BrandsRepository()

    suspend operator fun invoke() = repository.getAllBrands()
}