package com.example.ropa.ui.views.brands

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.ropa.databinding.FragmentBrandsBinding
import com.example.ropa.ui.viewModels.BrandsViewModel
import com.example.ropa.ui.views.adapters.BrandAdapter


class BrandsFragment : Fragment() {

    private var _binding : FragmentBrandsBinding?=null
    private val binding get() = _binding!!
    private val brandsViewModel : BrandsViewModel by viewModels()
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        _binding = FragmentBrandsBinding.inflate(inflater, container, false)
        brandsViewModel.loadBrands()
        brandsViewModel.brands.observe(viewLifecycleOwner, Observer { brands->
            binding.brandsRecycler.layoutManager= LinearLayoutManager(context)
            val adapter = BrandAdapter(brands)
            binding.brandsRecycler.adapter = adapter
            if(brands.isNotEmpty()){

                binding.brandsShimmer.visibility= View.GONE
                binding.brandsRecycler.visibility= View.VISIBLE
            }
        })
        brandsViewModel.isLoading.observe(viewLifecycleOwner, Observer {

            binding.brandsShimmer.isVisible=it
        })

        return binding.root
    }
}