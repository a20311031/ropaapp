package com.example.ropa.data.repositories

import com.example.ropa.data.models.brands.Brand
import com.example.ropa.data.models.brands.BrandProvider
import com.example.ropa.data.models.brands.BrandsProvider
import com.example.ropa.data.network.brands.BrandService

class BrandsRepository {
    private val api = BrandService()

    suspend fun getAllBrands() : List<Brand> {
        val response = api.getBrands()
        BrandsProvider.brands = response
        return response
    }

    suspend fun getBrand(brand_id: Int) : Brand {
        val response = api.getBrand(brand_id)
        BrandProvider.brand = response
        return response
    }

}