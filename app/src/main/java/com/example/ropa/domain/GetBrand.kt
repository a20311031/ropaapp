package com.example.ropa.domain

import com.example.ropa.data.repositories.BrandsRepository

class GetBrand {

    private val repository = BrandsRepository()

    suspend operator fun invoke(brand_id: Int) =
        repository.getBrand(brand_id)
}